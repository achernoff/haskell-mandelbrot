module OGL where

import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Data.IORef
import Mandelbrot

initGLUT = do
	(progname,_) <- getArgsAndInitialize
	initialDisplayMode $= [RGBMode, DoubleBuffered]
	createWindow $ "Mandelbrot " ++ progname
	windowSize $= Size 500 500

	-- callbacks

	idleCallback $= Just idle
	keyboardMouseCallback $= Nothing
	displayCallback $= display 

	mainLoop

idle = postRedisplay Nothing

display = do
	clear [ColorBuffer]
	drawMandelbrot
	swapBuffers

drawMandelbrot = renderPrimitive Points $ do
	mapM_ drawColoredPoint allPoints
	where
		drawColoredPoint (a,z) = do
			color z
			vertex a

allPoints = map (normalColor . normalVertex) $ mandelbrot 500 500 1000

normalVertex a = let ((x,y),z) = normalPoint a in (Vertex3 x y 0, z)

normalPoint ((x,y),z) = ((realToFrac (2*x/3 + 1/3)::GLfloat,realToFrac y::GLfloat),z)
--normalize = uncurry (uncurry (((,) .) . (,) . (1 / 3 +) . (/ 3) (2 *)))


normalColor (a,z) = let zz = realToFrac (z/1000)::GLfloat in (a, Color3 zz zz zz)
