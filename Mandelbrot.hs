module Mandelbrot (mandelbrot, outMap) where

import Control.Monad (ap)
import Data.List (unfoldr)
import Control.Parallel
import Control.Parallel.Strategies


-- ======================== CONSTANTS ========================

mandelbrot :: (Num b, Ord b) => Float -> Float -> b -> [ (( Float, Float ), b) ]
mandelbrot nx ny limit = map (mtuple $ escape limit) [ (a,b) | a <- genNumRange xMin xMax nx, b <- genNumRange yMin yMax ny ]

--mandelbrotPar :: (Num b, Ord b) => Float -> Float -> b -> [((Float,Float),b)]
--mandelbrotPar nx ny limit = parMap rdeepseq (mtuple $ escape limit) [ (a,b) | a <- genNumRange xMin xMax nx, b <- genNumRange yMin yMax ny ]

genNumRange :: (Enum t, Fractional t) => t -> t -> t -> [t]
genNumRange low high d = let step = (high - low) / (d - 1) in [low,low+step..high]

escape :: (Num a, Num b, Ord a, Ord b) => a -> (b, b) -> a
escape limit (x,y) = limit - escape' x y limit x y

escape' :: (Num a, Num b, Ord a, Ord b) =>  a -> a -> b -> a -> a -> b
escape' xi yi lim x y = if x*x + y*y < 4 && lim > 0
	then escape' xi yi (lim - 1) (x*x - y*y + xi) (2*x*y + yi)
	else lim

mtuple :: (a -> b) -> a -> (a, b)
mtuple f x = (x, f x)
--mtuple = ap (,)

-- ======================== CONSTANTS ========================

xMin,yMin,xMax,yMax :: Float
xMin = -2.5
xMax = 1
yMin = -1
yMax = 1

outMap :: String
outMap = [' '..'z']
