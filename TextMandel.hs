module Main (main) where

import Mandelbrot
import Data.List
import System.Environment (getArgs)
import Control.Parallel
import Control.Parallel.Strategies

-- ======================== MAIN ========================

main :: IO ()
main = do
	[a,b] <- getArgs
	let aa = read a :: Float
	    bb = read b :: Int
	    cc = read b :: Float
	putStrLn . unlines . parMap rpar concat $ (map . map) ((return . (!!) outMap) . snd) 
			$ groupIn bb $ mandelbrot aa cc (length outMap - 1)

-- ======================== PARSING ========================

groupIn :: Int -> [a] -> [[a]]
groupIn = unfoldr . splitMaybe
		
splitMaybe :: Int -> [a] -> Maybe ([a],[a])
splitMaybe y [] = Nothing
splitMaybe y x  = Just $ splitAt y x
-- splitMaybe = (Just .) . splitAt
